# My Brainfuck monorepo

## instructions
### build:
`cmake --build build/ -j 10`
### run tests:
`ctest -j100 -T test --output-on-failure --test-dir build`

## performance log
all measurements on my machine with
### mandelbrot
from https://rosettacode.org/wiki/Mandelbrot_set#Brainf.2A.2A.2A
- 3ab99741475c99b3f90190bcbfec911d2bc4a8bb with -Ofast         : 38.33s
- 3ab99741475c99b3f90190bcbfec911d2bc4a8bb with -O1    -DNDEBUG: 25.89s
- cf046499c70a068736272293113c2a6db0e3144d with -Ofast -DNDEBUG: 27.55s
- 181728c8bcee76a4e738656c0c32a30f8a7cfefd with -Ofast -DNDEBUG: 57.43s
- 0ddbdbd73f5e1fb7044673a1c3a2056fd4fd4d71 with -Ofast -DNDEBUG: 27.89s
- f9bb6cf60f575d0fb80ff68e1ae50957a60c188a with -Ofast -DNDEBUG: 26.47s
- 5291541d1f3ff52e91f245a1d194c9b268bcecf7 with -Os    -DNDEBUG: 21.76s
- 8361d2896fe257bb12b1225e48db36d1ca0b1827 with -Os    -DNDEBUG: 24.15s
- c1e18bb6ce6d37227365c94b23340c097d7f16fe with -Os    -DNDEBUG: 17.47s / 10521107970 instructions
- 9eda682ea4b6cf235130fa14dee79c408ca15a30 with -O1    -DNDEBUG:  6.30s /  3018468909 instructions
- 8998fe905238119c3065a45ce135edb00740ed32 with -O1    -DNDEBUG:  6.22s /  2994391927 instructions
- 2cfc6bfaf5fbef6d4ea3b8a124d5f59ccc707e20 with -O1    -DNDEBUG:  5.71s
- da6dff9b9232c0fd714374e2d4ebc14e94069828 with -O1    -DNDEBUG:  5.65s
- 28773edcb8d27d9e4574f5eafd57631b21196dfa with -O1    -DNDEBUG:  5.57s
- 79b2c24bf99218f994e07407da02610af97623f8 with -O1    -DNDEBUG:  5.82s
- 1800b3073bedb1d7f840107a9f5c3ab69f1cce8a with -O1    -DNDEBUG:  4.95s