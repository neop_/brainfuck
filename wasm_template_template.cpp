

#include <cstdio>

unsigned char mem[1'000'000] = {0};

int main() { auto* p = mem;
	*p += 13;
	while (*p) {
		*p -= 1;
		p += 1;
		*p += 2;
		p += 3;
		*p += 5;
		p += 1;
		*p += 2;
		p += 1;
		*p += 1;
		p -= 6;
	}
	std::putchar(*p);
	*p = std::getchar();
	p += 5;
	*p += 6;
	p += 1;
	*p -= 3;
	p += 10;
	*p += 15;
	while (*p) {
		while (*p) {
			p += 9;
		}
		*p += 1;
		while (*p) {
			p -= 9;
		}
		p += 9;
		*p -= 1;
	}
	*p += 1;
	while (*p) {
		p += 8;
		*p = 0;
		p += 1;
	}
}