
#include <array>
#include <iterator>
#include <sstream>
#include <string>
#include <string_view>
using namespace std::literals;

#include <catch2/catch.hpp>

#include "brainfuck/brainfuck.hpp"
namespace bf = de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc;

constexpr auto example_program = R"(
	+++++[> [-]>[-]>[-]>[-]>[-]>[-]>[-]>[-]>[-]>[-]<<<<<<<<<
	++++[++++>---<]>++.-[----->+<]>++++.--[->+++<]>-.++++++++++++.+++.----.-------.--[--->+<]>-.[-->+++++++<]>.++.---.--------.+++++++++++.+++[->+++<]>++.++++++++++++.[->+++++<]>-.+++++[->+++<]>.++++++.-.----.+++++.-.>++++++++++.
	<<<<<<<<<<-]
)"sv;

constexpr auto expected_output = R"(Example program output
Example program output
Example program output
Example program output
Example program output
)";

TEST_CASE("can interpret correct example program") {
	const auto code =
		neop::pump(example_program | bf::to_token(), bf::to_ir(bf::compile()));
	std::string output;
	bf::run(
		code, std::array<bf::DefaultCell, 1000>{}, neop::getchar_iterator{},
		std::back_inserter(output)
	);
	REQUIRE(output == expected_output);
}

TEST_CASE("can interpret correct example program with bounds checking") {
	const auto code =
		neop::pump(example_program | bf::to_token(), bf::to_ir(bf::compile()));
	std::string output;
	bf::run(
		code, bf::bounds_checked_adapter{std::array<bf::DefaultCell, 1000>{}},
		neop::getchar_iterator{}, std::back_inserter(output)
	);
	REQUIRE(output == expected_output);
}

TEST_CASE("can execute mandelbrot from disk") {
	const auto code = bf::read_code("../example_bf_programs/mandelbrot.bf");
	std::string output;
	bf::run(
		code, std::vector<bf::DefaultCell>(1'000'000), neop::getchar_iterator{},
		std::back_inserter(output)
	);

	const auto expected_output =
		[] {
			std::ifstream expected_output_file{
				"../example_bf_programs/mandelbrot.txt"};
			std::ostringstream s;
			s << expected_output_file.rdbuf();
			return s;
			// clang-format off
		}().str();
	// clang-format on
	REQUIRE(output == expected_output);
}

TEST_CASE("can correctly interpret empty program") {
	const auto code =
		neop::pump(""sv | bf::to_token(), bf::to_ir(bf::compile()));
	std::string output;
	bf::run(
		code, std::array<bf::DefaultCell, 1>{}, neop::getchar_iterator{},
		std::back_inserter(output)
	);
	REQUIRE(output.empty());
}

TEST_CASE("detects syntax errors during compilation") {
	constexpr auto too_many_opening =
		"there were more opening braces than closing braces.";
	constexpr auto too_many_closing =
		"encountered a closing brace without a matching opening brace.";

	CHECK_THROWS_WITH(
		neop::pump("["sv | bf::to_token(), bf::to_ir(bf::compile())),
		too_many_opening
	);

	CHECK_THROWS_WITH(
		neop::pump(
			"+[--[.[,,[.]-][]<><]][[[[][]]]"sv | bf::to_token(),
			bf::to_ir(bf::compile())
		),
		too_many_opening
	);

	CHECK_THROWS_WITH(
		neop::pump("]"sv | bf::to_token(), bf::to_ir(bf::compile())),
		too_many_closing
	);

	CHECK_THROWS_WITH(
		neop::pump(
			"-.,-.,[<><<[+-+[++[]][]]]][[[][]]]"sv | bf::to_token(),
			bf::to_ir(bf::compile())
		),
		too_many_closing
	);
}

TEST_CASE("going one step backward on the default bounds checking tape throws "
          "exception") {
	const auto code =
		neop::pump("<"sv | bf::to_token(), bf::to_ir(bf::compile()));

	std::string output;
	REQUIRE_THROWS_WITH(
		bf::run(
			code, bf::make_bounds_checking_static_tape(),
			neop::getchar_iterator{}, std::back_inserter(output)
		),
		"virtual machine went outside of the lower boundary"
	);
}

TEST_CASE("going one step forward on a single cell bounds checking tape throws "
          "exception") {
	const auto code =
		neop::pump(">"sv | bf::to_token(), bf::to_ir(bf::compile()));

	std::string output;
	REQUIRE_THROWS_WITH(
		bf::run(
			code, bf::bounds_checked_adapter{std::array<bf::DefaultCell, 1>{}},
			neop::getchar_iterator{}, std::back_inserter(output)
		),
		"virtual machine went outside of the upper boundary"
	);
}

TEST_CASE("running bf to fill the infinite tape with ones on a bounds checking "
          "tape") {
	const auto code =
		neop::pump("+[>+]"sv | bf::to_token(), bf::to_ir(bf::compile()));

	auto tape = bf::make_bounds_checking_static_tape();

	SECTION("throws exception and leaves the tape filled with ones") {
		const auto tapesize = std::ranges::size(tape.underlying);
		std::string output;
		REQUIRE_THROWS_WITH(
			bf::run(
				code, tape, neop::getchar_iterator{}, std::back_inserter(output)
			),
			"virtual machine went outside of the upper boundary"
		);

		REQUIRE(tape.underlying.size() == tapesize);
		REQUIRE(std::ranges::all_of(tape.underlying, [](auto cell) {
			return cell == 1;
		}));
	}
}
