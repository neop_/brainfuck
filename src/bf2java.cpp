

#include "brainfuck/transpiler_main.hpp"
namespace bf = de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc;

template <typename Os>
struct java_transpile_policy {
	static auto write_head(Os& os) {
		os << R"(public class Main{public static void main(String[] args)throws Exception{var tape = new byte[1000000];var i = 0;)";
	}
	static auto write_rled_ir(Os& os, bf::ir_point p, int count) {
		using enum bf::ir_point;
		switch (p) {
			case incr: os << "tape[i]+="; break;
			case decr: os << "tape[i]-="; break;
			case right: os << "i+="; break;
			case left: os << "i-="; break;
			default: UNREACHABLE();
		}
		os << count << ';';
	}
	static auto write_read(Os& os) { os << "tape[i]=(byte)System.in.read();"; }
	static auto write_write(Os& os) {
		os << "System.out.print((char)tape[i]);";
	}
	static auto write_zero(Os& os) { os << "tape[i]=0;"; }
	static auto write_whle(Os& os) { os << "while(tape[i]!=0){"; }
	static auto write_wend(Os& os) { os << '}'; }
	static auto write_tail(Os& os) { os << "}}"; }
};

int main(int argc, char* argv[]) {
	return bf::transpiler_main<java_transpile_policy>(argc, argv);
}