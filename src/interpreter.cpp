
#include "brainfuck/brainfuck.hpp"
namespace bf = de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc;

#include <cstdio>

auto main(int argc, char* argv[]) -> int try {
	bf::run(bf::read_code(argv[1]), bf::make_bounds_checking_static_tape());
} catch (const std::exception& e) {
	std::fputs(e.what(), stderr);
	std::fputc('\n', stderr);
	return EXIT_FAILURE;
}