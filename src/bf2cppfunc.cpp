

#include "brainfuck/transpiler_main.hpp"
namespace bf = de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc;

template <typename Os>
struct cpp_transpile_policy {
	static auto write_head(Os& os) {
		os << R"(
#pragma once
#include <brainfuck/brainfuck.hpp>
namespace {
namespace bf = de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc;
template <
	std::input_iterator Input = neop::getchar_iterator,
	typename Output = neop::putchar_iterator,
	std::ranges::random_access_range Tape = std::invoke_result_t<decltype([] {
		return bf::make_unchecked_static_tape();
	})>>
auto run(
	Tape&& tape = bf::make_unchecked_static_tape(),
	Input input = neop::getchar_iterator{},
	Output output = neop::putchar_iterator{})
	-> std::ranges::borrowed_iterator_t<Tape> {
	auto p = std::ranges::begin(tape);
)";
	}
	static auto write_rled_ir(Os& os, bf::ir_point p, int count) {
		using enum bf::ir_point;
		switch (p) {
			case incr: os << "*p+="; break;
			case decr: os << "*p-="; break;
			case right: os << "p+="; break;
			case left: os << "p-="; break;
			default: UNREACHABLE();
		}
		os << count << ';';
	}
	static auto write_read(Os& os) { os << "*p=*input++;"; }
	static auto write_write(Os& os) { os << "*output++=*p;"; }
	static auto write_zero(Os& os) { os << "*p=0;"; }
	static auto write_whle(Os& os) { os << "while(*p){"; }
	static auto write_wend(Os& os) { os << '}'; }
	static auto write_tail(Os& os) { os << R"(return p;}})"; }
};

int main(int argc, char* argv[]) {
	return bf::transpiler_main<cpp_transpile_policy>(argc, argv);
}