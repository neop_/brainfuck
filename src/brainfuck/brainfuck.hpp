#pragma once

#include <algorithm>
#include <cassert>
#include <chrono>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include <iterator>
#include <limits>
#include <memory>
#include <optional>
#include <ostream>
#include <random>
#include <ranges>
#include <stdexcept>
#include <type_traits>
#include <utility>

#define ever (;;)
#define FWD(...) ::std::forward<decltype(__VA_ARGS__)>(__VA_ARGS__)

#ifdef NDEBUG
	#if __has_builtin(__builtin_unreachable)
		#define UNREACHABLE() __builtin_unreachable()
	#else
		#define UNREACHABLE()                                                  \
			(*((int*)0) = 0) // TODO: bad unreachable impl; compiler just
			                 // generates instructions
	#endif
#else
	#define UNREACHABLE() assert((void("is unreachable"), false))
#endif

#define ASSUME(...)                                                            \
	[&] {                                                                      \
		if (!(__VA_ARGS__)) {                                                  \
			UNREACHABLE();                                                     \
		}                                                                      \
	}()

#define ASSERT(...) (assert(__VA_ARGS__), ASSUME(__VA_ARGS__))

#ifdef NDEBUG
	#define DEBUG_LOG(...) (void(0))
#else
	#define DEBUG_LOG(...) (std::clog << __VA_ARGS__)
#endif

namespace neop {
template <typename T, typename S>
concept remove_cvref_same_as = std::is_same_v<std::remove_cvref_t<T>, S>;

// overflows
inline constexpr auto ceiling_divide(unsigned long n, unsigned long d) {
	return (n + d - 1) / d;
}

template <typename Derived>
struct output_iterator_interface {
	// satisfy arbitrary requirement of weakly_incrementable >
	// input_or_output_iterator > output_iterator
	using difference_type = std::ptrdiff_t;
	using value_type = void;

private:
	Derived& derived() noexcept {
		static_assert(std::is_base_of_v<
					  output_iterator_interface<Derived>, Derived>);
		return static_cast<Derived&>(*this);
	}

public:
	constexpr auto& operator++() { return derived(); }
	// from std::output_iterator: *i++ = E; has effects equivalent to *i = E;
	// ++i; therefore we must return a reference in general. When using an
	// output iterator adaptor you don't want to work with a copy and discard it
	// immediately and loose the changes operator= did.
	constexpr auto& operator++(int) { return derived(); }
	constexpr auto& operator*() { return derived(); }
};

template <typename Container>
struct collect_to_container_iterator
	: neop::output_iterator_interface<
		  collect_to_container_iterator<Container>> {
	Container container;
	auto operator=(auto&& value) { container.push_back(FWD(value)); }
};

template <typename Out>
concept has_nested_output_it = requires(Out out) { out.out; };

decltype(auto) get_terminal_output_it(has_nested_output_it auto&& out) {
	return (get_terminal_output_it(FWD(out).out));
}

decltype(auto) get_terminal_output_it(auto&& out) { return (FWD(out)); }

namespace detail {
// only declared here to break dependency cycle with terminal_action_t
constexpr decltype(auto) terminal_action(has_nested_output_it auto&& out);
constexpr auto terminal_action(auto&& /*out*/) {
	struct default_noop_terminal_action_no_nested_out {};
	return default_noop_terminal_action_no_nested_out{};
}

using terminal_action_t = decltype([](auto&& out) -> decltype(auto) {
	return terminal_action(FWD(out));
});
} // namespace detail

inline constexpr detail::terminal_action_t terminal_action{};

namespace detail {
constexpr decltype(auto) terminal_action(has_nested_output_it auto&& out) {
	return neop::terminal_action(FWD(out).out);
}
} // namespace detail

constexpr auto pump(auto&& range, auto&& out) {
	return neop::terminal_action(std::ranges::copy(FWD(range), FWD(out)).out);
}

struct getchar_iterator {
	using difference_type = std::ptrdiff_t;
	using value_type = int;

	getchar_iterator& operator++() { return *this; }
	auto operator++(int) const { return *this; }
	auto operator*() const { return std::getchar(); }
};

struct assign_to_putchar {
	auto operator=(int c) const { std::putchar(c); }
};

struct putchar_iterator {
	using difference_type = std::ptrdiff_t;
	using value_type = int;
	auto& operator++() { return *this; }
	auto operator++(int) const { return *this; }
	auto operator*() { return assign_to_putchar{}; }
};

namespace detail {
auto assign(auto&& range, auto&& output_iterator) {
	std::ranges::copy(FWD(range), FWD(output_iterator));
}

using assign_t = decltype([](auto&& range, auto&& output_iterator) {
	assign(FWD(range), FWD(output_iterator));
});
} // namespace detail

/*
CPO that can be used when writing a known range to an output iterator.
The default customization above simply copies the range one by one as you would
do without this CPO. When customized, this can improve performance, for example
when inserting a sized range at the end of a vector the capacity theoretically
(qoi) needs to be checked only once alternative design would be a new concept
that requires assign() but that would make existing output iterators
incompatible and there is no need to require assign, since a good default can be
specified.
*/
inline constexpr auto assign = detail::assign_t{};

template <typename Range, typename Value>
concept input_range_of = std::ranges::input_range<Range> &&
                         std::same_as<std::ranges::range_value_t<Range>, Value>;

template <typename Range, typename Value>
concept contiguous_range_of =
	std::ranges::contiguous_range<Range> &&
	std::same_as<std::ranges::range_value_t<Range>, Value>;

template <typename Range, typename Value>
concept random_access_range_of =
	std::ranges::random_access_range<Range> &&
	std::same_as<std::ranges::range_value_t<Range>, Value>;

} // namespace neop

auto assign(
	auto&& range, std::back_insert_iterator<std::vector<auto>> back_inserter
) {
	struct container_getter_t : decltype(back_inserter) {
		auto& get() { return *decltype(back_inserter)::container; }
	};

	const auto& container = container_getter_t{back_inserter}.get();

	container.insert(
		container.end(), std::ranges::begin(range), std::ranges::end(range)
	);
}

namespace de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc {

// a range of those represents the source code as is. comments are squashed. No
// unnamed values are valid.
enum struct token : unsigned char {
	comment,
	incr,
	decr,
	right,
	left,
	read,
	write,
	whle,
	wend,
};

// intermediate representation. has no more comments. Simple compression
// optimizations that can be easily reversed are applied here. Suitable for
// translation to higher languages.
enum struct ir_point : unsigned char {
	incr,
	decr,
	right,
	left,
	read,
	write,
	whle,
	wend,
	zero,
};

// this is the bytecode run by the implemented bf vm. specifically jump indices
// are encoded in the instruction stream.
enum struct instruction : unsigned char {
	incr,
	decr,
	right,
	left,
	read,
	write,
	whle,
	wend,
	zero,
	end_sentinel,
	// shaves off like 5s (??):
	// incr='+', decr= '-', right='>', left='<', read=',', write='.', whle='[',
	// wend=']', spacer
};

inline namespace from_char {
constexpr auto lookup = [] {
	std::array<
		token, std::numeric_limits<unsigned char>::max() +
				   1 /*wait a minute, is this an instant overflow?*/>
		result{};
	result['+'] = token::incr;
	result['-'] = token::decr;
	result['>'] = token::right;
	result['<'] = token::left;
	result[','] = token::read;
	result['.'] = token::write;
	result['['] = token::whle;
	result[']'] = token::wend;
	return result;
}();

inline constexpr auto to_token(char c) {
	return lookup[static_cast<unsigned char>(c)];
}
} // namespace from_char

inline constexpr auto to_token() {
	return std::views::transform(from_char::to_token);
}

inline auto token_view(std::istream& is) {
	return std::ranges::istream_view<char>(is) | to_token();
}

auto debug_print(neop::input_range_of<instruction> auto&& code) {
	for (auto ins : code) {
		using enum instruction;
		switch (ins) {
			case incr: std::clog << '+'; break;
			case decr: std::clog << '-'; break;
			case right: std::clog << '>'; break;
			case left: std::clog << '<'; break;
			case read: std::clog << ','; break;
			case write: std::clog << '.'; break;
			case whle: std::clog << '['; break;
			case wend: std::clog << ']'; break;
			case zero: std::clog << 'z'; break;
			case end_sentinel: std::clog << "(end)"; break;
			default:
				std::clog << '('
						  << unsigned(static_cast<unsigned char>(
								 std::to_underlying(ins)
							 ))
						  << ')';
		}
	}
	std::clog << "\n}}\n";
}

auto debug_print(neop::input_range_of<ir_point> auto&& code) {
	for (auto ins : code) {
		using enum ir_point;
		switch (ins) {
			case incr: std::clog << '+'; break;
			case decr: std::clog << '-'; break;
			case right: std::clog << '>'; break;
			case left: std::clog << '<'; break;
			case read: std::clog << ','; break;
			case write: std::clog << '.'; break;
			case whle: std::clog << '['; break;
			case wend: std::clog << ']'; break;
			case zero: std::clog << 'z'; break;
			default:
				std::clog << '('
						  << unsigned(static_cast<unsigned char>(
								 std::to_underlying(ins)
							 ))
						  << ')';
		}
	}
	std::clog << "\n}}\n";
}

constexpr auto count_of_instructions_to_store(std::size_t size) {
	return neop::ceiling_divide(size, sizeof(instruction));
}

auto calculate_jumps(neop::contiguous_range_of<instruction> auto& code) {
	// make space for jump sizes beforehands and then calculate jumps as normal
	// without any offset adjusting
	std::vector<std::ptrdiff_t> while_begin_idx;
	for (auto i = std::ranges::begin(code); i != std::ranges::end(code); ++i) {
		switch (*i) {
			case instruction::whle:
				while_begin_idx.push_back(i - std::ranges::cbegin(code));
				i += count_of_instructions_to_store(sizeof(std::ptrdiff_t));
				break;

			case instruction::wend: {
				std::ptrdiff_t while_end_index = i - std::ranges::cbegin(code);
				ASSERT(!while_begin_idx.empty());
				std::memcpy(
					std::to_address(std::ranges::next(
						std::ranges::begin(code) + while_begin_idx.back()
					)),
					&while_end_index, sizeof(std::ptrdiff_t)
				);
				std::memcpy(
					std::to_address(std::ranges::next(i)),
					&while_begin_idx.back(), sizeof(std::ptrdiff_t)
				);
				while_begin_idx.pop_back();
				i += count_of_instructions_to_store(sizeof(std::ptrdiff_t));
			} break;

			case instruction::incr:
			case instruction::decr:
			case instruction::right:
			case instruction::left:
				// skip encoded run length for rled instructions
				++i;
				break;

			default:;
		}
	}

	ASSERT(while_begin_idx.empty());
}

template <typename Out>
struct rle_t : neop::output_iterator_interface<rle_t<Out>> {
private:
	struct Run {
		ir_point type;
		std::underlying_type_t<ir_point> length;
	};

public:
	Out out;
	std::optional<Run> run_state;
	constexpr auto operator=(ir_point p) {
		switch (p) {
			case ir_point::incr:
			case ir_point::decr:
			case ir_point::right:
			case ir_point::left: {
				if (run_state) {
					if (run_state->type == p &&
					    run_state->length <
					        std::numeric_limits<decltype(Run::length)>::max()) {
						++run_state->length;
						break;
					}

					// there was a run going on and we are going to start a new
					// one -> finish previous.
					*out++ = ir_point{run_state->length};
				}
				run_state = Run{.type = p, .length = 1};
				*out++ = p;
			} break;

			case ir_point::read:
			case ir_point::write:
			case ir_point::whle:
			case ir_point::wend:
			case ir_point::zero: {
				if (run_state)
					[this](auto& run) {
						*out++ = ir_point{run.length};
						run_state.reset();
					}(*run_state);
				*out++ = p;
			} break;

			default: UNREACHABLE();
		}
	}
};
namespace detail {
template <typename T>
constexpr auto is_rle_t = false;
template <typename Out>
constexpr auto is_rle_t<rle_t<Out>> = true;
} // namespace detail
template <neop::has_nested_output_it /*contrained with nested_out to
                                        disambiguate with default that is only
                                        contrained with nested_out*/
              T>
	requires detail::is_rle_t<T>
auto terminal_action(T&& out) {
	if (out.run_state) {
		*out.out++ = ir_point{out.run_state->length};
		out.run_state.reset();
	}
	return neop::terminal_action(FWD(out).out);
}
constexpr auto rle(auto&& out) { return rle_t{.out = FWD(out)}; }

template <typename Out>
struct inspect_t : neop::output_iterator_interface<inspect_t<Out>> {
	Out out;
	auto operator=(token value) {
		std::clog << int(std::to_underlying(value)) << ",";
		*out++ = FWD(value);
	}
};
auto inspect(auto&& out) { return inspect_t{.out = FWD(out)}; }

template <typename Out>
struct match_set_zero_t
	: neop::output_iterator_interface<match_set_zero_t<Out>> {
private:
	enum class State {
		Nothing,
		GotWhile,
		GotDecr,
	};

public:
	Out out;
	State state = State::Nothing;
	constexpr auto operator=(ir_point p) {
		using enum ir_point;
		switch (state) {
			break;
			case State::Nothing:
				switch (p) {
					case whle: state = State::GotWhile; break;

					case incr:
					case decr:
					case right:
					case left:
					case read:
					case write:
					case wend: *out++ = p; break;

					default:
						UNREACHABLE(); // no spacers or encoded values expected
						               // here
				};
				break;
			case State::GotWhile:
				switch (p) {
					case decr: state = State::GotDecr; break;

					case whle: *out++ = whle; break;

					case incr:
					case right:
					case left:
					case read:
					case write:
					case wend: {
						*out++ = whle;
						*out++ = p;
						state = State::Nothing;
					} break;

					default:
						UNREACHABLE(); // no spacers or encoded values expected
						               // here
				};
				break;
			case State::GotDecr:
				switch (p) {
					case wend: {
						*out++ = zero;
						state = State::Nothing;
					} break;

					case whle: {
						*out++ = whle;
						*out++ = decr;
						state = State::GotWhile;
					} break;

					case incr:
					case decr:
					case right:
					case left:
					case read:
					case write: {
						*out++ = whle;
						*out++ = decr;
						*out++ = p;
						state = State::Nothing;
					} break;
					default:
						DEBUG_LOG("offender: " << int(std::to_underlying(p)));
						UNREACHABLE(); // no spacers or encoded values expected
						               // here
				};
		}
	}
};
constexpr auto match_set_zero(auto&& out) {
	return match_set_zero_t{.out = FWD(out)};
}

static_assert(std::is_aggregate_v<
			  rle_t<std::back_insert_iterator<std::vector<int>>>>);

inline constexpr auto non_comment(token t) { return t != token::comment; }

inline constexpr auto no_comment_to_ir(token t) {
	switch (t) {
		case token::incr: return ir_point::incr;
		case token::decr: return ir_point::decr;
		case token::right: return ir_point::right;
		case token::left: return ir_point::left;
		case token::read: return ir_point::read;
		case token::write: return ir_point::write;
		case token::whle: return ir_point::whle;
		case token::wend: return ir_point::wend;
		default: UNREACHABLE();
	}
}

template <typename Out>
struct to_unprocessed_ir_t
	: neop::output_iterator_interface<to_unprocessed_ir_t<Out>> {
	Out out;
	constexpr auto operator=(token t) {
		if (non_comment(t)) {
			*out++ = no_comment_to_ir(t);
		}
	}
};

template <typename Out>
struct assert_syntax_t : neop::output_iterator_interface<assert_syntax_t<Out>> {
	Out out;
	long nesting_level = 0;
	constexpr auto operator=(ir_point p) {
		using enum ir_point;
		ASSERT(
			p == whle or p == wend or p == incr or p == decr or p == right or
			p == left or p == read or p == write
		);
		switch (p) {
			case whle: {
				++nesting_level;
			} break;
			case wend: {
				if (nesting_level == 0) {
					throw std::runtime_error{
						"encountered a closing brace without a matching "
						"opening brace."};
				}
				--nesting_level;
			} break;

			default:;
		}
		*out++ = p;
	}
};
namespace detail {
template <typename T>
constexpr auto is_assert_syntax_t = false;
template <typename Out>
constexpr auto is_assert_syntax_t<assert_syntax_t<Out>> = true;
} // namespace detail
template <neop::has_nested_output_it /*contrained with nested_out to
                                        disambiguate with default that is only
                                        contrained with nested_out*/
              T>
	requires detail::is_assert_syntax_t<T> decltype(auto)
terminal_action(T&& out) {
	if (out.nesting_level != 0) {
		throw std::runtime_error{
			"there were more opening braces than closing braces."};
	}
	return neop::terminal_action(FWD(out).out);
}

constexpr auto to_ir(auto&& out) {
	return to_unprocessed_ir_t{
		.out = assert_syntax_t{.out = match_set_zero(rle(FWD(out)))}};
}

[[deprecated(
	"exposition only, should not be used by user and is unused in library"
	" but demonstrates non_comment and no_comment_to_ir."
)]] inline auto
to_unprocessed_ir() {
	return std::views::filter(non_comment) |
	       std::views::transform(no_comment_to_ir);
}

template <std::ranges::random_access_range VectorLike>
struct compile_t : neop::output_iterator_interface<compile_t<VectorLike>> {
	VectorLike instructions;
	int items_with_encoded_length = 0;
	auto operator=(ir_point t) {
		if (items_with_encoded_length > 0) {
			instructions.push_back(instruction{std::to_underlying(t)});
			--items_with_encoded_length;
			return;
		}

		switch (t) {
			case ir_point::incr:
				instructions.push_back(instruction::incr);
				items_with_encoded_length = 1;
				break;
			case ir_point::decr:
				instructions.push_back(instruction::decr);
				items_with_encoded_length = 1;
				break;
			case ir_point::right:
				instructions.push_back(instruction::right);
				items_with_encoded_length = 1;
				break;
			case ir_point::left:
				instructions.push_back(instruction::left);
				items_with_encoded_length = 1;
				break;

			case ir_point::read:
				instructions.push_back(instruction::read);
				break;
			case ir_point::write:
				instructions.push_back(instruction::write);
				break;
			case ir_point::zero:
				instructions.push_back(instruction::zero);
				break;

			case ir_point::whle: {
				instructions.push_back(instruction::whle);
				instructions.resize(
					instructions.size() +
					count_of_instructions_to_store(sizeof(std::ptrdiff_t))
				);
			} break;
			case ir_point::wend: {
				instructions.push_back(instruction::wend);
				instructions.resize(
					instructions.size() +
					count_of_instructions_to_store(sizeof(std::ptrdiff_t))
				);
			} break;

			default: {
				DEBUG_LOG("offender: " << int(std::to_underlying(t)));
				UNREACHABLE();
			}
		}
	}
};
namespace detail {
template <typename T>
constexpr auto is_compile_t = false;
template <typename VectorLike>
constexpr auto is_compile_t<compile_t<VectorLike>> = true;
static_assert(is_compile_t<compile_t<std::vector<instruction>>>);
} // namespace detail
template <typename T>
	requires detail::is_compile_t<T>
auto terminal_action(T&& compile) {
	compile.instructions.push_back(instruction::end_sentinel);
	calculate_jumps(compile.instructions);
	return FWD(compile).instructions;
}

inline auto compile() {
	return compile_t{.instructions = std::vector<instruction>{}};
}

inline auto read_code(const char* const fname) {
	if (fname == nullptr) {
		throw std::runtime_error{"no file given"};
	}

	std::ifstream code_file{fname};
	if (!code_file) {
		throw std::runtime_error{std::string{"could not open file "} + fname};
	}

	return neop::pump(token_view(code_file), to_ir(compile()));
}

struct NoopProfiler {
	auto operator()(std::ptrdiff_t) {}
};

// only used for the bf vm. Currently not safe in general. Also by no means a
// general solution anyways.
template <typename It>
struct checked_iterator {
	decltype(auto) operator*() const { return *underlying; }
	decltype(auto) operator*() { return *underlying; }

private:
	static auto upper_bound_violation() {
		return std::runtime_error{
			"virtual machine went outside of the upper boundary"};
	}
	static auto lower_bound_violation() {
		return std::runtime_error{
			"virtual machine went outside of the lower boundary"};
	}
	auto verify_upper_bound() const {
		if (index >= size) {
			throw upper_bound_violation();
		}
	}
	auto verify_lower_bound() const {
		if (index < 0) {
			throw lower_bound_violation();
		}
	}
	auto verify_upper_bound(std::ptrdiff_t absolute_index) const {
		if (absolute_index >= size) {
			throw upper_bound_violation();
		}
	}
	auto verify_lower_bound(std::ptrdiff_t absolute_index) const {
		if (absolute_index < 0) {
			throw lower_bound_violation();
		}
	}

public:
	auto& operator++() {
		++index;
		verify_upper_bound();
		++underlying;
		return *this;
	}
	auto operator++(int) {
		auto temp = *this;
		++*this;
		return temp;
	}
	auto& operator--() {
		--index;
		verify_lower_bound();
		--underlying;
		return *this;
	}
	auto operator--(int) {
		auto temp = *this;
		--*this;
		return temp;
	}
	auto& operator+=(std::ptrdiff_t n) {
		ASSERT(n >= 0);
		index += n;
		verify_upper_bound();
		underlying += n;
		return *this;
	}
	auto& operator-=(std::ptrdiff_t n) {
		ASSERT(n >= 0);
		index -= n;
		verify_lower_bound();
		underlying -= n;
		return *this;
	}
	friend auto operator+(const checked_iterator& lhs, std::ptrdiff_t n) {
		ASSERT(n >= 0);
		lhs.verify_upper_bound(lhs.index + n);
		return checked_iterator{
			.underlying = lhs.underlying + n,
			.size = lhs.size,
			.index = lhs.index + n};
	}
	friend auto operator-(const checked_iterator& lhs, std::ptrdiff_t n) {
		ASSERT(n >= 0);
		lhs.verify_lower_bound(lhs.index - n);
		return checked_iterator{
			.underlying = lhs.underlying - n,
			.size = lhs.size,
			.index = lhs.index - n};
	}
	friend auto operator+(std::ptrdiff_t n, const checked_iterator& rhs) {
		ASSERT(n >= 0);
		rhs.verify_upper_bound(rhs.index + n);
		return checked_iterator{
			.underlying = rhs.underlying + n,
			.size = rhs.size,
			.index = rhs.index + n};
	}
	decltype(auto) operator[](std::ptrdiff_t i) {
		ASSERT(i >= 0);
		verify_upper_bound(index + i);
		return underlying[i];
	}
	decltype(auto) operator[](std::ptrdiff_t i) const {
		ASSERT(i >= 0);
		verify_upper_bound(index + i);
		return underlying[i];
	}
	auto operator-(const checked_iterator<auto>& rhs) const {
		return underlying - rhs.underlying;
	}

	using difference_type = std::ptrdiff_t;
	using value_type = std::iter_value_t<It>;

	auto operator<=>(const checked_iterator<auto>& rhs) const {
		return underlying <=> rhs.underlying;
	}
	auto operator==(const checked_iterator<auto>& rhs) const {
		return underlying == rhs.underlying;
	}

	It underlying;
	std::ptrdiff_t size;
	std::ptrdiff_t index;
};
auto checked_end(std::ranges::range auto&& underlying) {
	return checked_iterator{
		.underlying = std::ranges::end(underlying),
		.size = std::ranges::ssize(underlying),
		.index = std::ranges::ssize(underlying) - 1};
}
auto checked_begin(std::ranges::range auto&& underlying) {
	return checked_iterator{
		.underlying = std::ranges::begin(underlying),
		.size = std::ranges::ssize(underlying),
		.index = 0};
}

using DefaultCell = unsigned char;
constexpr auto default_tape_size = 1'048'576;

inline auto make_unchecked_static_tape(std::size_t size = default_tape_size) {
	return std::vector<DefaultCell>(size);
}

template <std::ranges::range R>
struct bounds_checked_adapter {
	R underlying;
	auto begin() const { return checked_begin(underlying); }
	auto end() const { return checked_end(underlying); }
	auto begin() { return checked_begin(underlying); }
	auto end() { return checked_end(underlying); }
};

inline auto
make_bounds_checking_static_tape(std::size_t size = default_tape_size) {
	return bounds_checked_adapter{make_unchecked_static_tape(size)};
}

#define GOTO_INSTRUCTION()                                                     \
	switch (*ip) {                                                             \
		case instruction::incr: goto L_incr;                                   \
		case instruction::decr: goto L_decr;                                   \
		case instruction::right: goto L_right;                                 \
		case instruction::left: goto L_left;                                   \
		case instruction::read: goto L_read;                                   \
		case instruction::write: goto L_write;                                 \
		case instruction::whle: goto L_whle;                                   \
		case instruction::wend: goto L_wend;                                   \
		case instruction::zero: goto L_zero;                                   \
		case instruction::end_sentinel: goto L_end_sentinel;                   \
		default: UNREACHABLE();                                                \
	}

#define GOTO_NEXT_INSTRUCTION()                                                \
	/* for the time based profilers to record accurately it is critical that   \
	 * the profiler is executed after the current instruction has actually     \
	 * run.*/                                                                  \
	profiler(current_instruction_idx);                                         \
	++ip;                                                                      \
	GOTO_INSTRUCTION()

template <
	std::input_iterator Input = neop::getchar_iterator,
	std::output_iterator<char /*concretisation, maybe refrain from output_it*/>
		Output = neop::putchar_iterator,
	std::ranges::random_access_range Tape = std::invoke_result_t<decltype([] {
		return make_unchecked_static_tape();
	})>,
	typename Profiler = NoopProfiler>
auto run(
	neop::contiguous_range_of<instruction> auto&& code,
	Tape&& tape = make_unchecked_static_tape(),
	Input input = neop::getchar_iterator{},
	Output output = neop::putchar_iterator{},
	Profiler&& profiler = NoopProfiler{}
) -> std::ranges::borrowed_iterator_t<Tape> {
	ASSERT(*std::ranges::crbegin(code) == instruction::end_sentinel);

	auto p = std::ranges::begin(tape);
	const auto code_begin = std::ranges::cbegin(code);
	auto ip = code_begin;

	GOTO_INSTRUCTION();
	{
	L_incr : {
		const auto current_instruction_idx = ip - code_begin;
		*p += std::to_underlying(*++ip);
		GOTO_NEXT_INSTRUCTION();
	}
	L_decr : {
		const auto current_instruction_idx = ip - code_begin;
		*p -= std::to_underlying(*++ip);
		GOTO_NEXT_INSTRUCTION();
	}
	L_right : {
		const auto current_instruction_idx = ip - code_begin;
		p += std::to_underlying(*++ip);
		GOTO_NEXT_INSTRUCTION();
	}
	L_left : {
		const auto current_instruction_idx = ip - code_begin;
		p -= std::to_underlying(*++ip);
		GOTO_NEXT_INSTRUCTION();
	}
	L_read : {
		const auto current_instruction_idx = ip - code_begin;
		*p = *input++;
		GOTO_NEXT_INSTRUCTION();
	}
	L_write : {
		const auto current_instruction_idx = ip - code_begin;
		*output++ = *p;
		GOTO_NEXT_INSTRUCTION();
	}
	L_whle : {
		const auto current_instruction_idx = ip - code_begin;
		if (!*p) {
			std::ptrdiff_t jump_to_index;
			std::memcpy(
				&jump_to_index, std::to_address(std::ranges::next(ip)),
				sizeof(std::ptrdiff_t)
			);
			ip = code_begin + jump_to_index;
		}
		ip += count_of_instructions_to_store(sizeof(std::ptrdiff_t));
		GOTO_NEXT_INSTRUCTION();
	}
	L_wend : {
		const auto current_instruction_idx = ip - code_begin;
		if (*p) {
			std::ptrdiff_t jump_to_index;
			std::memcpy(
				&jump_to_index, std::to_address(std::ranges::next(ip)),
				sizeof(std::ptrdiff_t)
			);
			ip = code_begin + jump_to_index;
		}
		ip += count_of_instructions_to_store(sizeof(std::ptrdiff_t));
		GOTO_NEXT_INSTRUCTION();
	}
	L_zero : {
		const auto current_instruction_idx = ip - code_begin;
		*p = 0;
		GOTO_NEXT_INSTRUCTION();
	}
	L_end_sentinel : { return p; }
	}
}
#undef GOTO_NEXT_INSTRUCTION
#undef GOTO_INSTRUCTION
} // namespace de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc
