#pragma once
#include "brainfuck/brainfuck.hpp"

#include <functional>
#include <optional>

namespace de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc {
template <typename Os, typename transpile_policy>
struct transpile_t
	: neop::output_iterator_interface<transpile_t<Os, transpile_policy>> {
	explicit transpile_t(Os& os) : os{os} { tp.write_head(os); }
	auto operator=(ir_point p) {
		if (encountered_rled_ir) {
			using enum ir_point;
			ASSERT(
				*encountered_rled_ir == incr or *encountered_rled_ir == decr or
				*encountered_rled_ir == right or *encountered_rled_ir == left
			);
			tp.write_rled_ir(os, *encountered_rled_ir, std::to_underlying(p));
			encountered_rled_ir = std::nullopt;
			return;
		}

		switch (p) {
			case ir_point::incr:
			case ir_point::decr:
			case ir_point::right:
			case ir_point::left: encountered_rled_ir = p; break;

			case ir_point::read: tp.write_read(os); break;
			case ir_point::write: tp.write_write(os); break;
			case ir_point::zero: tp.write_zero(os); break;
			case ir_point::whle: tp.write_whle(os); break;
			case ir_point::wend: tp.write_wend(os); break;

			default: {
				DEBUG_LOG("offender: " << int(std::to_underlying(p)));
				UNREACHABLE();
			}
		}
	}

	auto write_tail() { tp.write_tail(os); }

	friend auto
	terminal_action(neop::remove_cvref_same_as<transpile_t> auto&& out) {
		out.tp.write_tail(out.os);
	}

private:
	std::reference_wrapper<Os> os;
	std::optional<ir_point> encountered_rled_ir;
	[[no_unique_address]] transpile_policy tp;
};
} // namespace de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc