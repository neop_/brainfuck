#pragma once
#include "brainfuck/brainfuck.hpp"

namespace de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc::compiletime_code {

template <std::array text>
requires std::is_same_v<typename decltype(text)::value_type, char>
constexpr
auto to_tokens() {
	constexpr auto tokens = text | to_token();
	constexpr auto result_size = std::ranges::count_if(tokens, non_comment);
	std::array<token, result_size> filtered_tokens;
	std::ranges::copy_if(tokens, filtered_tokens.begin(), non_comment);
	return filtered_tokens;
}

namespace types {
	template <std::size_t count>
	struct incr {};
	template <std::size_t count>
	struct decr {};
	template <std::size_t count>
	struct right {};
	template <std::size_t count>
	struct left {};
	struct read {};
	struct write {};
	struct whle {};
	struct wend {};
	template <typename... Nested>
	struct program {};
	template <typename... Nested>
	struct CompleteWhile {};
} // namespace types

template <std::size_t count> auto run_(types::incr<count>,  auto& p, auto& i, auto& o) { *p += count; }
template <std::size_t count> auto run_(types::decr<count>,  auto& p, auto& i, auto& o) { *p -= count; }
template <std::size_t count> auto run_(types::right<count>, auto& p, auto& i, auto& o) { p += count;  }
template <std::size_t count> auto run_(types::left<count>,  auto& p, auto& i, auto& o) { p -= count;  }
auto run_(types::read,  auto& p, auto& i, auto& o) { *p = *i++; }
auto run_(types::write, auto& p, auto& i, auto& o) { *o++ = *p; }

template <typename... Nested>
auto run_(types::CompleteWhile<Nested...>, auto& p, auto& i, auto& o) {
	while (*p) {
		(void(run_(Nested{}, p, i, o)), ...);
	}
}
template <typename... Nested>
auto run_(types::program<Nested...>, auto& p, auto& i, auto& o) {
	(void(run_(Nested{}, p, i, o)), ...);
}

template <
	typename Code,
	std::input_iterator Input = neop::getchar_iterator, 
	std::output_iterator<char/*concretisation, maybe refrain from output_it*/> Output = neop::putchar_iterator,
	std::ranges::random_access_range Tape = std::vector<unsigned char>,
	typename Profiler = NoopProfiler
> auto run(
	Code code,
	Tape&& tape = std::vector<unsigned char>(1'000'000),
	Input input = neop::getchar_iterator{},
	Output output = neop::putchar_iterator{}
) {
	auto p = std::ranges::begin(tape);
	run_(Code{}, p, input, output);
	return p;
}

template <typename... first, typename... second>
constexpr
auto concat(types::program<first...>, types::program<second...>) {
	return types::program<first..., second...>{};
}

template <std::size_t count_first, std::size_t count_second, typename... tail>
constexpr
auto concat(types::program<types::incr<count_first>>, types::program<types::incr<count_second>, tail...>) {
	return types::program<types::incr<count_first + count_second>, tail...>{};
}

template <std::size_t count_first, std::size_t count_second, typename... tail>
constexpr
auto concat(types::program<types::decr<count_first>>, types::program<types::decr<count_second>, tail...>) {
	return types::program<types::decr<count_first + count_second>, tail...>{};
}

template <std::size_t count_first, std::size_t count_second, typename... tail>
constexpr
auto concat(types::program<types::left<count_first>>, types::program<types::left<count_second>, tail...>) {
	return types::program<types::left<count_first + count_second>, tail...>{};
}

template <std::size_t count_first, std::size_t count_second, typename... tail>
constexpr
auto concat(types::program<types::right<count_first>>, types::program<types::right<count_second>, tail...>) {
	return types::program<types::right<count_first + count_second>, tail...>{};
}

template <typename Ast, typename Remaining>
struct compile_result {
	Ast ast;
	Remaining remaining;
};

template <typename T, std::size_t n>
constexpr
auto tail(const std::array<T, n>& array) {
	static_assert(n >= 1);
	std::array<T, n - 1> result;
	std::ranges::copy(array.begin() + 1, array.end(), result.begin());
	return result;
}

template <typename... Nested>
constexpr
auto as_while(types::program<Nested...>) {
	return types::CompleteWhile<Nested...>{};
}

template <std::array tokens>
requires std::is_same_v<typename decltype(tokens)::value_type, token>
constexpr
auto compile_() {
	// all those elses are needed so the compiler is not confused regarding front when array empty and the like

	if constexpr (tokens.empty()) {
		return compile_result{types::program<>{}, tokens};
	} else

	if constexpr (tokens.front() == token::incr ) { constexpr auto res = compile_<tail(tokens)>(); return compile_result{concat(types::program<types::incr<1>>{},  res.ast), res.remaining}; } else
	if constexpr (tokens.front() == token::decr ) { constexpr auto res = compile_<tail(tokens)>(); return compile_result{concat(types::program<types::decr<1>>{},  res.ast), res.remaining}; } else
	if constexpr (tokens.front() == token::right) { constexpr auto res = compile_<tail(tokens)>(); return compile_result{concat(types::program<types::right<1>>{}, res.ast), res.remaining}; } else
	if constexpr (tokens.front() == token::left ) { constexpr auto res = compile_<tail(tokens)>(); return compile_result{concat(types::program<types::left<1>>{},  res.ast), res.remaining}; } else
	if constexpr (tokens.front() == token::read ) { constexpr auto res = compile_<tail(tokens)>(); return compile_result{concat(types::program<types::read>{},  res.ast), res.remaining}; } else
	if constexpr (tokens.front() == token::write) { constexpr auto res = compile_<tail(tokens)>(); return compile_result{concat(types::program<types::write>{}, res.ast), res.remaining}; } else

	if constexpr (tokens.front() == token::whle) {
		constexpr auto res_while = compile_<tail(tokens)>();
		static_assert(not res_while.remaining.empty() && res_while.remaining.front() == token::wend, "more opening than closing braces");
		constexpr auto res_after_while = compile_</*drop closing brace*/tail(res_while.remaining)>();
		return compile_result{concat(types::program<decltype(as_while(res_while.ast))>{}, res_after_while.ast), res_after_while.remaining/*is this always empty?*/};
	} else
	// can always match epsilon without consuming anything:
	return compile_result{types::program<>{}, tokens};
}

template <std::array tokens>
requires std::is_same_v<typename decltype(tokens)::value_type, token>
constexpr
auto compile() {
	constexpr auto res = compile_<tokens>();
	static_assert(res.remaining.empty(), "more closing than opening braces");
	return res.ast;
}
} // namespace de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc::compiletime_code

namespace de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc::literals {
namespace detail {
template <std::size_t N>
struct ToArray {
	std::array<char, N> chars{};

	constexpr ToArray(const char (&pp)[N]) {
		std::ranges::copy(pp, chars.begin());
	}
};
} // namespace detail

template <detail::ToArray array>
constexpr auto operator"" _bf() {
	return compiletime_code::compile<compiletime_code::to_tokens<array.chars>()>();
}
} // namespace de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc::literals