#pragma once

#include "brainfuck/brainfuck.hpp"
#include "brainfuck/transpiling.hpp"

#include <fstream>
#include <iostream>

namespace de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc {
template <template <typename> typename transpile_policy>
int transpiler_main(int argc, char* argv[]) try {
	using to_transpiled =
		transpile_t<std::ostream, transpile_policy<std::ostream>>;

	auto translate = [](std::istream& is) {
		neop::pump(token_view(is), to_ir(to_transpiled{std::cout}));
	};

	if (argc == 2) {
		std::ifstream code_file{argv[1], std::ios::in};
		if (!code_file) {
			throw std::runtime_error{"could not open file"};
		}
		translate(code_file);
	} else {
		translate(std::cin);
	}
	return EXIT_SUCCESS;
} catch (const std::exception& e) {
	std::puts(e.what());
	return EXIT_FAILURE;
}
} // namespace de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc