#pragma once
#include "brainfuck/brainfuck.hpp"

#if __has_include(<pcg/pcg_random.hpp>) && __has_include(<pcg/pcg_extras.hpp>)
#include <pcg/pcg_extras.hpp>
#include <pcg/pcg_random.hpp>
#endif

#include <chrono>
#include <random>
#include <ranges>

namespace de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc {

auto generate_perf_html(
	neop::random_access_range_of<instruction> auto&& code,
	std::ranges::random_access_range auto&& instruction_frequency,
	std::ostream& os = std::cout
) -> std::ostream& {
	ASSERT(std::ranges::size(code) == std::ranges::size(instruction_frequency));

	const auto maximum_frequency = std::ranges::max(instruction_frequency);
	os << R"(<!DOCTYPE html><html><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>Document</title></head><body style="font-family: monospace;"><pre>)";
	auto indent = 0;
	for (auto i = 0z; i < std::ranges::ssize(code); ++i) {
		os << R"(<span style="background-color: rgb(255 )"
			<< std::min(255 * 2 - int(instruction_frequency[i] * 255.0 * 2 / maximum_frequency), 255)
			<< ' '
			<< std::max(255     - int(instruction_frequency[i] * 255.0 * 2 / maximum_frequency), 0)
			<< ")\">";
		switch (code[i]) {
			using enum instruction;

			break;case incr : ++i; for (auto j = 0z; j < std::to_underlying(code[i]); ++j) { os << '+';   }
			break;case decr : ++i; for (auto j = 0z; j < std::to_underlying(code[i]); ++j) { os << '-';   }
			break;case right: ++i; for (auto j = 0z; j < std::to_underlying(code[i]); ++j) { os << "&gt"; }
			break;case left : ++i; for (auto j = 0z; j < std::to_underlying(code[i]); ++j) { os << "&lt"; }
			break;case read : os << ',';
			break;case write: os << '.';
			break;case whle : os << '\n' << std::string(indent++, '\t'); os << "[\n" << std::string(indent, '\t'); i += count_of_instructions_to_store(sizeof(std::ptrdiff_t));
			break;case wend : os << '\n' << std::string(--indent, '\t'); os << "]\n" << std::string(indent, '\t'); i += count_of_instructions_to_store(sizeof(std::ptrdiff_t));
			break;case zero : os << "[-]";
			break;case end_sentinel: ;
			break;default: UNREACHABLE();
		}
		os << "</span>";
	}
	os << "</pre></body></html>";
	return os;
}

struct SimpleInstructionCountingProfiler {
	explicit SimpleInstructionCountingProfiler(
		std::ranges::random_access_range auto&& code)
		: instruction_frequency(std::ranges::ssize(code)) {}
	std::vector<long> instruction_frequency;
	auto operator()(std::ptrdiff_t idx) { ++instruction_frequency[idx]; }
};

struct TimeBasedProfiler {
	explicit TimeBasedProfiler(std::ranges::random_access_range auto&& code)
		: instruction_frequency(std::ranges::ssize(code)) {}

	std::vector<long> instruction_frequency;
	auto operator()(const std::ptrdiff_t i) {
		const auto now = std::chrono::steady_clock::now();
		instruction_frequency[i] +=
			std::chrono::nanoseconds{now - last_call_time}.count();
		last_call_time = now;
	}

  private:
	std::chrono::steady_clock::time_point last_call_time =
		std::chrono::steady_clock::now();
};

#if __has_include(<pcg/pcg_random.hpp>) && __has_include(<pcg/pcg_extras.hpp>)
struct StatisticalTimeBasedProfiler {
	explicit StatisticalTimeBasedProfiler(
		std::ranges::random_access_range auto&& code)
		: instruction_frequency(std::ranges::ssize(code)) {}

	std::vector<long> instruction_frequency;
	auto operator()(const std::ptrdiff_t i) {
		if (start_time) {
			auto end_time = std::chrono::steady_clock::now();
			instruction_frequency[i] +=
				std::chrono::nanoseconds{end_time - *start_time}.count();

			if (should_measure_next(rne)) {
				start_time = end_time;
			} else {
				start_time = std::nullopt;
			}
		} else {
			if (should_measure_next(rne)) {
				start_time = std::chrono::steady_clock::now();
			}
		}
	}

  private:
	pcg32 rne{pcg_extras::seed_seq_from<std::random_device>{}};
	std::bernoulli_distribution should_measure_next{0.001};
	std::optional<std::chrono::steady_clock::time_point> start_time;
};
#endif

} // namespace de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc