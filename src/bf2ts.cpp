

#include "brainfuck/transpiler_main.hpp"
namespace bf = de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc;

template <typename Os>
struct deno_ts_transpile_policy {
	static auto write_head(Os& os) {
		os << R"("use strict";import{writeAllSync}from"https://deno.land/std/streams/conversion.ts";let tape = new Uint8Array(30000);let c = new Uint8Array(1);let i = 0;)";
	}
	static auto write_rled_ir(Os& os, bf::ir_point p, int count) {
		using enum bf::ir_point;
		switch (p) {
			case incr: os << "tape[i]+="; break;
			case decr: os << "tape[i]-="; break;
			case right: os << "i+="; break;
			case left: os << "i-="; break;
			default: UNREACHABLE();
		}
		os << count << ';';
	}
	static auto write_read(Os& os) { os << "Deno.stdin.read(c);tape[i]=c[0];"; }
	static auto write_write(Os& os) {
		os << "writeAllSync(Deno.stdout, new Uint8Array([tape[i]]));";
	}
	static auto write_zero(Os& os) { os << "tape[i]=0;"; }
	static auto write_whle(Os& os) { os << "while(tape[i]!==0){"; }
	static auto write_wend(Os& os) { os << '}'; }
	static auto write_tail(Os& os) {}
};

template <typename Os>
struct bun_ts_transpile_policy {
	static auto write_head(Os& os) {
		os << R"("use strict";/*let output = Promise.resolve()*/;let tape = new Uint8Array(30000);let c = new Uint8Array(1);let i = 0;)";
	}
	static auto write_rled_ir(Os& os, bf::ir_point p, int count) {
		using enum bf::ir_point;
		switch (p) {
			case incr: os << "tape[i]+="; break;
			case decr: os << "tape[i]-="; break;
			case right: os << "i+="; break;
			case left: os << "i-="; break;
			default: UNREACHABLE();
		}
		os << count << ';';
	}
	static auto write_read(Os& os) {
		os << "await Deno.stdin.read(c);tape[i]=c[0];";
	}
	static auto write_write(Os& os) {
		// clang-format off
		// all these variations pass the test but have different performance
		// on the regular mandelbrot, the difference becomes neglible
		// benchmarks done with bun version 0.7.0, hyperfine, dull_mandelbrot.bf

		// Time (mean ± σ):     226.6 ms ±   7.2 ms    [User: 208.4 ms, System: 25.0 ms]
  		// Range (min … max):   217.6 ms … 243.2 ms    13 runs
		// os << "{let char = tape[i]; output = output.then(() => {return Bun.write(Bun.stdout, new Uint8Array([char]));});}";

		// Time (mean ± σ):      1.816 s ±  0.022 s    [User: 1.833 s, System: 0.040 s]
		// Range (min … max):    1.797 s …  1.866 s    10 runs
		// os << "await Bun.write(Bun.stdout, new Uint8Array([tape[i]]));";

		// Time (mean ± σ):      85.0 ms ±   1.8 ms    [User: 68.1 ms, System: 15.8 ms]
		// Range (min … max):    82.7 ms …  90.2 ms    34 runs
		// this variation was broken in earlier versions, as the writes were unordered with respect to eachother.
		// The current bun versions appears to order them
		os << "Bun.write(Bun.stdout, new Uint8Array([tape[i]]));";
		// clang-format on
	}
	static auto write_zero(Os& os) { os << "tape[i]=0;"; }
	static auto write_whle(Os& os) { os << "while(tape[i]!==0){"; }
	static auto write_wend(Os& os) { os << '}'; }
	static auto write_tail(Os& os) {}
};

int main(int argc, char* argv[]) {
	return bf::transpiler_main<bun_ts_transpile_policy>(argc, argv);
}