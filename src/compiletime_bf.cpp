
#include "brainfuck/compiletime_code.hpp"
namespace bf = de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc;

using namespace bf::literals;
constexpr auto program =
	R"(>++++++++[<+++++++++>-]<.>>+>+>++>[-]+<[>[->+<<++++>]<<]>.+++++++..+++.>>+++++++.<<<[[-]<[-]>]<+++++++++++++++.>>.+++.------.--------.>>+.>++++.)"_bf;

int main() {
	namespace cbf = bf::compiletime_code;
	cbf::run(program);
}