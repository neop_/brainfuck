

#include "brainfuck/transpiler_main.hpp"
namespace bf = de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc;

#include <vector>

template <typename Os>
struct x86_transpile_policy {
	std::vector<int> while_labels;
	int label_counter = 0;

	static auto write_head(Os& os) {
		os << R"(
	.intel_syntax noprefix
	.text
	.globl	main
main:
	sub rsp, 1000000
	mov edx, 1000000
	mov esi, 0
	mov rdi, rsp
	call memset
	mov rbx, rsp)";
	}
	static auto write_rled_ir(Os& os, bf::ir_point p, int count) {
		using enum bf::ir_point;
		switch (p) {
			case incr:
				os << R"(
	movzx eax, BYTE PTR [rbx]
	add eax, )" << count
				   << R"(
	mov BYTE PTR [rbx], al)";
				break;
			case decr:
				os << R"(
	movzx eax, BYTE PTR [rbx]
	sub eax, )" << count
				   << R"(
	mov BYTE PTR [rbx], al)";
				break;
			case right:
				os << R"(
	add rbx, )" << count;
				break;
			case left:
				os << R"(
	sub rbx, )" << count;
				break;
			default: UNREACHABLE();
		}
	}
	static auto write_read(Os& os) { os << "unimplemented"; }
	static auto write_write(Os& os) {
		os << R"(
	movsx edi, BYTE PTR [rbx]
	mov rsi, QWORD PTR stdout[rip]
	call putc)";
	}
	static auto write_zero(Os& os) { os << "\nmov BYTE PTR [rbx], 0"; }
	auto write_whle(Os& os) {
		while_labels.push_back(label_counter);
		os << R"(
	cmp BYTE PTR [rbx], 0
	jz WAFTER_)"
		   << label_counter << "\nWSTART_" << label_counter << ':';
		++label_counter;
	}
	auto write_wend(Os& os) {
		os << R"(
WEND_)" << while_labels.back()
		   << R"(:
	cmp BYTE PTR [rbx], 0
	jne WSTART_)"
		   << while_labels.back() << R"(
WAFTER_)" << while_labels.back()
		   << ':';
		while_labels.pop_back();
	}
	static auto write_tail(Os& os) {
		os << R"(
	xor eax, eax
	add rsp, 1000000
	ret
)";
	}
};

int main(int argc, char* argv[]) {
	return bf::transpiler_main<x86_transpile_policy>(argc, argv);
}