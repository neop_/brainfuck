#include <cstdio>
#include <fstream>
#include <stdexcept>
#include <string>
#include <vector>

auto calculate_jumps(std::string_view code) {
	using idx_t = std::ptrdiff_t;
	std::vector<idx_t> jump_indices(code.size());
	std::vector<idx_t> while_begin_idx;

	for (auto i = 0z; i < std::ranges::ssize(code); ++i) {
		switch (code[i]) {
			case '[': {
				while_begin_idx.push_back(i);
			} break;

			case ']': {
				const auto wbi = while_begin_idx.back();
				jump_indices[i] = wbi;
				jump_indices[wbi] = i;
				while_begin_idx.pop_back();
			} break;

			default:;
		}
	}
	return jump_indices;
}

auto get_code(const char* fname) {
	std::ifstream code_file{fname, std::ios::in};
	if (!code_file) {
		throw std::runtime_error{"could not open file"};
	}
	code_file.seekg(0, std::ios::end);
	std::string code(code_file.tellg(), 0);
	code_file.seekg(0);
	code_file.read(code.data(), std::ranges::ssize(code));
	return code;
}

int main(int argc, char* argv[]) try {
	if (argc != 2) {
		throw std::runtime_error{"Usage: tiny <bf-file>"};
	}
	const auto code = get_code(argv[1]);
	const auto jump_indices = calculate_jumps(code);

	unsigned char tape[30'000] = {0};
	auto p = std::ranges::begin(tape);
	const auto code_begin = std::ranges::cbegin(code);
	const auto code_end = std::ranges::cend(code);

	for (auto ip = code_begin; ip != code_end; ++ip) {
		switch (*ip) {
			case '+': ++*p; break;
			case '-': --*p; break;
			case '>': ++p; break;
			case '<': --p; break;
			case ',': *p = std::getchar(); break;
			case '.': std::putchar(*p); break;
			case '[': {
				if (!*p) {
					ip = code_begin + jump_indices[ip - code_begin];
				}
			} break;
			case ']': {
				if (*p) {
					ip = code_begin + jump_indices[ip - code_begin];
				}
			} break;

			default:;
		}
	}
} catch (const std::exception& e) {
	std::puts(e.what());
	return EXIT_FAILURE;
}