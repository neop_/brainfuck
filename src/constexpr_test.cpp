#include "brainfuck/brainfuck.hpp"
namespace bf = de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc;

#include <array>

constexpr auto mandelbrot = std::to_array(
#include "example_bf_programs/mandelbrot.ipp"
);

consteval auto get_code() {
	std::array<bf::instruction, 5> code{};
	neop::pump(mandelbrot | bf::to_token(), bf::compile(code.begin()));
	return code;
}

int main() {
	constexpr auto code = get_code();
	bf::run(code);
}