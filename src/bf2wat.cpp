

#include "brainfuck/transpiler_main.hpp"
namespace bf = de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc;

template <typename Os>
struct wat_transpile_policy {
	static auto write_head(Os& os) {
		os << R"wasm(
(module
  (type (;0;) (func (result i32)))
  (type (;1;) (func (param i32) (result i32)))
  (type (;2;) (func))
  (type (;3;) (func (param i32)))
  (type (;4;) (func (param i32 i32 i32) (result i32)))
  (type (;5;) (func (param i32 i64 i32) (result i64)))
  (type (;6;) (func (param i32 i32 i32 i32) (result i32)))
  (type (;7;) (func (param i32 i32) (result i32)))
  (type (;8;) (func (param i32 i64 i32 i32) (result i32)))
  (import "wasi_snapshot_preview1" "proc_exit" (func $__wasi_proc_exit (type 3)))
  (import "wasi_snapshot_preview1" "fd_close" (func $__wasi_fd_close (type 1)))
  (import "wasi_snapshot_preview1" "fd_read" (func $__wasi_fd_read (type 6)))
  (import "wasi_snapshot_preview1" "fd_seek" (func $__wasi_fd_seek (type 8)))
  (import "wasi_snapshot_preview1" "fd_write" (func $__wasi_fd_write (type 6)))
  (func $__wasm_call_ctors (type 2)
    call $init_pthread_self)
  (func $__original_main (type 0) (result i32)
    (local $p i32)
    i32.const 1328
    local.set $p

)wasm";
	}
	static auto write_rled_ir(Os& os, bf::ir_point p, int count) {
		using enum bf::ir_point;
		switch (p) {
			case incr:
				os << R"(
    local.get $p
    local.get $p
    i32.load8_u
    i32.const )" << count
				   << R"(
    i32.add
    i32.store8)";
				break;
			case decr:
				os << R"(
    local.get $p
    local.get $p
    i32.load8_u
    i32.const )" << count
				   << R"(
    i32.sub
    i32.store8)";
				break;
			case right:
				os << R"(
    local.get $p
    i32.const )" << count
				   << R"(
    i32.add
    local.set $p)";
				break;
			case left:
				os << R"(
    local.get $p
    i32.const )" << count
				   << R"(
    i32.sub
    local.set $p)";
				break;
			default: UNREACHABLE();
		}
	}
	static auto write_read(Os& os) {
		os << R"(
    local.get $p
    call $getchar
    i32.store8)";
	}
	static auto write_write(Os& os) {
		os << R"(
    local.get $p
    i32.load8_u
    call $putchar
    drop)";
	}
	static auto write_zero(Os& os) {
		os << R"(
    local.get $p
    i32.const 0
    i32.store8)";
	}
	auto write_whle(Os& os) {
		os << R"(
    local.get $p
    i32.load8_u
    if
      loop)";
	}
	auto write_wend(Os& os) {
		os << R"(
        local.get $p
        i32.load8_u
        br_if 0
      end
    end)";
	}
	static auto write_tail(Os& os) {
		os << R"wasm(
    i32.const 0)
  (func $_start (type 2)
    call $__wasm_call_ctors
    call $__original_main
    call $exit
    unreachable)
  (func $dummy (type 2)
    nop)
  (func $libc_exit_fini (type 2)
    call $dummy)
  (func $exit (type 3) (param i32)
    call $dummy
    call $libc_exit_fini
    call $__stdio_exit
    local.get 0
    call $_Exit
    unreachable)
  (func $_Exit (type 3) (param i32)
    local.get 0
    call $__wasi_proc_exit
    unreachable)
  (func $__lockfile (type 1) (param i32) (result i32)
    i32.const 1)
  (func $__stdio_exit (type 2)
    (local i32)
    call $__ofl_lock
    i32.load
    local.tee 0
    if  ;; label = @1
      loop  ;; label = @2
        local.get 0
        call $close_file
        local.get 0
        i32.load offset=56
        local.tee 0
        br_if 0 (;@2;)
      end
    end
    i32.const 1168
    i32.load
    call $close_file
    i32.const 1320
    i32.load
    call $close_file
    i32.const 1001328
    i32.load
    call $close_file)
  (func $close_file (type 3) (param i32)
    (local i32 i32)
    block  ;; label = @1
      local.get 0
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.load offset=76
      i32.const 0
      i32.ge_s
      if  ;; label = @2
        local.get 0
        call $__lockfile
        drop
      end
      local.get 0
      i32.load offset=20
      local.get 0
      i32.load offset=28
      i32.ne
      if  ;; label = @2
        local.get 0
        i32.const 0
        i32.const 0
        local.get 0
        i32.load offset=36
        call_indirect (type 4)
        drop
      end
      local.get 0
      i32.load offset=4
      local.tee 1
      local.get 0
      i32.load offset=8
      local.tee 2
      i32.eq
      br_if 0 (;@1;)
      local.get 0
      local.get 1
      local.get 2
      i32.sub
      i64.extend_i32_s
      i32.const 1
      local.get 0
      i32.load offset=40
      call_indirect (type 5)
      drop
    end)
  (func $__toread (type 1) (param i32) (result i32)
    (local i32 i32)
    local.get 0
    local.get 0
    i32.load offset=72
    local.tee 1
    i32.const 1
    i32.sub
    local.get 1
    i32.or
    i32.store offset=72
    local.get 0
    i32.load offset=20
    local.get 0
    i32.load offset=28
    i32.ne
    if  ;; label = @1
      local.get 0
      i32.const 0
      i32.const 0
      local.get 0
      i32.load offset=36
      call_indirect (type 4)
      drop
    end
    local.get 0
    i32.const 0
    i32.store offset=28
    local.get 0
    i64.const 0
    i64.store offset=16
    local.get 0
    i32.load
    local.tee 1
    i32.const 4
    i32.and
    if  ;; label = @1
      local.get 0
      local.get 1
      i32.const 32
      i32.or
      i32.store
      i32.const -1
      return
    end
    local.get 0
    local.get 0
    i32.load offset=44
    local.get 0
    i32.load offset=48
    i32.add
    local.tee 2
    i32.store offset=8
    local.get 0
    local.get 2
    i32.store offset=4
    local.get 1
    i32.const 27
    i32.shl
    i32.const 31
    i32.shr_s)
  (func $__uflow (type 1) (param i32) (result i32)
    (local i32 i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 1
    global.set $__stack_pointer
    i32.const -1
    local.set 2
    block  ;; label = @1
      local.get 0
      call $__toread
      br_if 0 (;@1;)
      local.get 0
      local.get 1
      i32.const 15
      i32.add
      i32.const 1
      local.get 0
      i32.load offset=32
      call_indirect (type 4)
      i32.const 1
      i32.ne
      br_if 0 (;@1;)
      local.get 1
      i32.load8_u offset=15
      local.set 2
    end
    local.get 1
    i32.const 16
    i32.add
    global.set $__stack_pointer
    local.get 2)
  (func $getchar (type 0) (result i32)
    call $do_getc)
  (func $do_getc (type 0) (result i32)
    (local i32)
    block  ;; label = @1
      i32.const 1100
      i32.load
      local.tee 0
      i32.const 0
      i32.ge_s
      if  ;; label = @2
        local.get 0
        i32.eqz
        br_if 1 (;@1;)
        call $__get_tp
        i32.load offset=24
        local.get 0
        i32.const -1073741825
        i32.and
        i32.ne
        br_if 1 (;@1;)
      end
      i32.const 1028
      i32.load
      local.tee 0
      i32.const 1032
      i32.load
      i32.ne
      if  ;; label = @2
        i32.const 1028
        local.get 0
        i32.const 1
        i32.add
        i32.store
        local.get 0
        i32.load8_u
        return
      end
      i32.const 1024
      call $__uflow
      return
    end
    call $locking_getc)
  (func $locking_getc (type 0) (result i32)
    (local i32)
    call $a_cas
    if  ;; label = @1
      i32.const 1024
      call $__lockfile
      drop
    end
    block (result i32)  ;; label = @1
      i32.const 1028
      i32.load
      local.tee 0
      i32.const 1032
      i32.load
      i32.ne
      if  ;; label = @2
        i32.const 1028
        local.get 0
        i32.const 1
        i32.add
        i32.store
        local.get 0
        i32.load8_u
        br 1 (;@1;)
      end
      i32.const 1024
      call $__uflow
    end
    local.set 0
    call $a_swap
    i32.const 1073741824
    i32.and
    if  ;; label = @1
      call $__wake
    end
    local.get 0)
  (func $a_cas (type 0) (result i32)
    (local i32)
    i32.const 1100
    i32.const 1100
    i32.load
    local.tee 0
    i32.const 1073741823
    local.get 0
    select
    i32.store
    local.get 0)
  (func $a_swap (type 0) (result i32)
    (local i32)
    i32.const 1100
    i32.load
    local.set 0
    i32.const 1100
    i32.const 0
    i32.store
    local.get 0)
  (func $__wake (type 2)
    i32.const 1100
    i32.const 1
    call $emscripten_futex_wake
    drop)
  (func $__errno_location (type 0) (result i32)
    i32.const 1001332)
  (func $emscripten_futex_wake (type 7) (param i32 i32) (result i32)
    i32.const 0)
  (func $__lock (type 3) (param i32)
    nop)
  (func $__ofl_lock (type 0) (result i32)
    i32.const 1001336
    call $__lock
    i32.const 1001340)
  (func $__syscall_getpid (type 0) (result i32)
    i32.const 42)
  (func $getpid (type 0) (result i32)
    call $__syscall_getpid)
  (func $__get_tp (type 0) (result i32)
    i32.const 1001400)
  (func $init_pthread_self (type 2)
    i32.const 1001496
    i32.const 1001376
    i32.store
    i32.const 1001424
    call $getpid
    i32.store)
  (func $__towrite (type 1) (param i32) (result i32)
    (local i32)
    local.get 0
    local.get 0
    i32.load offset=72
    local.tee 1
    i32.const 1
    i32.sub
    local.get 1
    i32.or
    i32.store offset=72
    local.get 0
    i32.load
    local.tee 1
    i32.const 8
    i32.and
    if  ;; label = @1
      local.get 0
      local.get 1
      i32.const 32
      i32.or
      i32.store
      i32.const -1
      return
    end
    local.get 0
    i64.const 0
    i64.store offset=4 align=4
    local.get 0
    local.get 0
    i32.load offset=44
    local.tee 1
    i32.store offset=28
    local.get 0
    local.get 1
    i32.store offset=20
    local.get 0
    local.get 1
    local.get 0
    i32.load offset=48
    i32.add
    i32.store offset=16
    i32.const 0)
  (func $__overflow (type 7) (param i32 i32) (result i32)
    (local i32 i32 i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    local.get 3
    local.get 1
    i32.store8 offset=15
    block  ;; label = @1
      local.get 0
      i32.load offset=16
      local.tee 2
      i32.eqz
      if  ;; label = @2
        i32.const -1
        local.set 2
        local.get 0
        call $__towrite
        br_if 1 (;@1;)
        local.get 0
        i32.load offset=16
        local.set 2
      end
      block  ;; label = @2
        local.get 0
        i32.load offset=20
        local.tee 4
        local.get 2
        i32.eq
        br_if 0 (;@2;)
        local.get 1
        i32.const 255
        i32.and
        local.tee 2
        local.get 0
        i32.load offset=80
        i32.eq
        br_if 0 (;@2;)
        local.get 0
        local.get 4
        i32.const 1
        i32.add
        i32.store offset=20
        local.get 4
        local.get 1
        i32.store8
        br 1 (;@1;)
      end
      i32.const -1
      local.set 2
      local.get 0
      local.get 3
      i32.const 15
      i32.add
      i32.const 1
      local.get 0
      i32.load offset=36
      call_indirect (type 4)
      i32.const 1
      i32.ne
      br_if 0 (;@1;)
      local.get 3
      i32.load8_u offset=15
      local.set 2
    end
    local.get 3
    i32.const 16
    i32.add
    global.set $__stack_pointer
    local.get 2)
  (func $putchar (type 1) (param i32) (result i32)
    local.get 0
    call $do_putc)
  (func $do_putc (type 1) (param i32) (result i32)
    (local i32 i32)
    block  ;; label = @1
      i32.const 1252
      i32.load
      local.tee 1
      i32.const 0
      i32.ge_s
      if  ;; label = @2
        local.get 1
        i32.eqz
        br_if 1 (;@1;)
        call $__get_tp
        i32.load offset=24
        local.get 1
        i32.const -1073741825
        i32.and
        i32.ne
        br_if 1 (;@1;)
      end
      block  ;; label = @2
        local.get 0
        i32.const 255
        i32.and
        local.tee 1
        i32.const 1256
        i32.load
        i32.eq
        br_if 0 (;@2;)
        i32.const 1196
        i32.load
        local.tee 2
        i32.const 1192
        i32.load
        i32.eq
        br_if 0 (;@2;)
        i32.const 1196
        local.get 2
        i32.const 1
        i32.add
        i32.store
        local.get 2
        local.get 0
        i32.store8
        local.get 1
        return
      end
      i32.const 1176
      local.get 1
      call $__overflow
      return
    end
    local.get 0
    call $locking_putc)
  (func $locking_putc (type 1) (param i32) (result i32)
    (local i32 i32)
    call $a_cas.1
    if  ;; label = @1
      i32.const 1176
      call $__lockfile
      drop
    end
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        i32.const 255
        i32.and
        local.tee 1
        i32.const 1256
        i32.load
        i32.eq
        br_if 0 (;@2;)
        i32.const 1196
        i32.load
        local.tee 2
        i32.const 1192
        i32.load
        i32.eq
        br_if 0 (;@2;)
        i32.const 1196
        local.get 2
        i32.const 1
        i32.add
        i32.store
        local.get 2
        local.get 0
        i32.store8
        br 1 (;@1;)
      end
      i32.const 1176
      local.get 1
      call $__overflow
      local.set 1
    end
    call $a_swap.1
    i32.const 1073741824
    i32.and
    if  ;; label = @1
      call $__wake.1
    end
    local.get 1)
  (func $a_cas.1 (type 0) (result i32)
    (local i32)
    i32.const 1252
    i32.const 1252
    i32.load
    local.tee 0
    i32.const 1073741823
    local.get 0
    select
    i32.store
    local.get 0)
  (func $a_swap.1 (type 0) (result i32)
    (local i32)
    i32.const 1252
    i32.load
    local.set 0
    i32.const 1252
    i32.const 0
    i32.store
    local.get 0)
  (func $__wake.1 (type 2)
    i32.const 1252
    i32.const 1
    call $emscripten_futex_wake
    drop)
  (func $dummy.1 (type 1) (param i32) (result i32)
    local.get 0)
  (func $__stdio_close (type 1) (param i32) (result i32)
    local.get 0
    i32.load offset=60
    call $dummy.1
    call $__wasi_fd_close)
  (func $__stdio_read (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i32)
    global.get $__stack_pointer
    i32.const 32
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    local.get 3
    local.get 1
    i32.store offset=16
    local.get 3
    local.get 2
    local.get 0
    i32.load offset=48
    local.tee 4
    i32.const 0
    i32.ne
    i32.sub
    i32.store offset=20
    local.get 0
    i32.load offset=44
    local.set 6
    local.get 3
    local.get 4
    i32.store offset=28
    local.get 3
    local.get 6
    i32.store offset=24
    i32.const 32
    local.set 4
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        i32.load offset=60
        local.get 3
        i32.const 16
        i32.add
        i32.const 2
        local.get 3
        i32.const 12
        i32.add
        call $__wasi_fd_read
        call $__wasi_syscall_ret
        i32.eqz
        if  ;; label = @3
          local.get 3
          i32.load offset=12
          local.tee 4
          i32.const 0
          i32.gt_s
          br_if 1 (;@2;)
          i32.const 32
          i32.const 16
          local.get 4
          select
          local.set 4
        end
        local.get 0
        local.get 0
        i32.load
        local.get 4
        i32.or
        i32.store
        br 1 (;@1;)
      end
      local.get 4
      local.set 5
      local.get 4
      local.get 3
      i32.load offset=20
      local.tee 6
      i32.le_u
      br_if 0 (;@1;)
      local.get 0
      local.get 0
      i32.load offset=44
      local.tee 5
      i32.store offset=4
      local.get 0
      local.get 5
      local.get 4
      local.get 6
      i32.sub
      i32.add
      i32.store offset=8
      local.get 0
      i32.load offset=48
      if  ;; label = @2
        local.get 0
        local.get 5
        i32.const 1
        i32.add
        i32.store offset=4
        local.get 1
        local.get 2
        i32.add
        i32.const 1
        i32.sub
        local.get 5
        i32.load8_u
        i32.store8
      end
      local.get 2
      local.set 5
    end
    local.get 3
    i32.const 32
    i32.add
    global.set $__stack_pointer
    local.get 5)
  (func $__lseek (type 5) (param i32 i64 i32) (result i64)
    (local i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    local.get 0
    local.get 1
    local.get 2
    i32.const 255
    i32.and
    local.get 3
    i32.const 8
    i32.add
    call $__wasi_fd_seek
    call $__wasi_syscall_ret
    local.set 2
    local.get 3
    i64.load offset=8
    local.set 1
    local.get 3
    i32.const 16
    i32.add
    global.set $__stack_pointer
    i64.const -1
    local.get 1
    local.get 2
    select)
  (func $__stdio_seek (type 5) (param i32 i64 i32) (result i64)
    local.get 0
    i32.load offset=60
    local.get 1
    local.get 2
    call $__lseek)
  (func $__stdio_write (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32)
    global.get $__stack_pointer
    i32.const 32
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    local.get 3
    local.get 0
    i32.load offset=28
    local.tee 4
    i32.store offset=16
    local.get 0
    i32.load offset=20
    local.set 5
    local.get 3
    local.get 2
    i32.store offset=28
    local.get 3
    local.get 1
    i32.store offset=24
    local.get 3
    local.get 5
    local.get 4
    i32.sub
    local.tee 1
    i32.store offset=20
    local.get 1
    local.get 2
    i32.add
    local.set 6
    local.get 3
    i32.const 16
    i32.add
    local.set 4
    i32.const 2
    local.set 7
    block (result i32)  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 0
            i32.load offset=60
            local.get 3
            i32.const 16
            i32.add
            i32.const 2
            local.get 3
            i32.const 12
            i32.add
            call $__wasi_fd_write
            call $__wasi_syscall_ret
            if  ;; label = @5
              local.get 4
              local.set 5
              br 1 (;@4;)
            end
            loop  ;; label = @5
              local.get 6
              local.get 3
              i32.load offset=12
              local.tee 1
              i32.eq
              br_if 2 (;@3;)
              local.get 1
              i32.const 0
              i32.lt_s
              if  ;; label = @6
                local.get 4
                local.set 5
                br 4 (;@2;)
              end
              local.get 4
              local.get 1
              local.get 4
              i32.load offset=4
              local.tee 8
              i32.gt_u
              local.tee 9
              i32.const 3
              i32.shl
              i32.add
              local.tee 5
              local.get 1
              local.get 8
              i32.const 0
              local.get 9
              select
              i32.sub
              local.tee 8
              local.get 5
              i32.load
              i32.add
              i32.store
              local.get 4
              i32.const 12
              i32.const 4
              local.get 9
              select
              i32.add
              local.tee 4
              local.get 4
              i32.load
              local.get 8
              i32.sub
              i32.store
              local.get 6
              local.get 1
              i32.sub
              local.set 6
              local.get 0
              i32.load offset=60
              local.get 5
              local.tee 4
              local.get 7
              local.get 9
              i32.sub
              local.tee 7
              local.get 3
              i32.const 12
              i32.add
              call $__wasi_fd_write
              call $__wasi_syscall_ret
              i32.eqz
              br_if 0 (;@5;)
            end
          end
          local.get 6
          i32.const -1
          i32.ne
          br_if 1 (;@2;)
        end
        local.get 0
        local.get 0
        i32.load offset=44
        local.tee 1
        i32.store offset=28
        local.get 0
        local.get 1
        i32.store offset=20
        local.get 0
        local.get 1
        local.get 0
        i32.load offset=48
        i32.add
        i32.store offset=16
        local.get 2
        br 1 (;@1;)
      end
      local.get 0
      i32.const 0
      i32.store offset=28
      local.get 0
      i64.const 0
      i64.store offset=16
      local.get 0
      local.get 0
      i32.load
      i32.const 32
      i32.or
      i32.store
      i32.const 0
      local.tee 1
      local.get 7
      i32.const 2
      i32.eq
      br_if 0 (;@1;)
      drop
      local.get 2
      local.get 5
      i32.load offset=4
      i32.sub
    end
    local.set 1
    local.get 3
    i32.const 32
    i32.add
    global.set $__stack_pointer
    local.get 1)
  (func $__emscripten_stdout_close (type 1) (param i32) (result i32)
    i32.const 0)
  (func $__emscripten_stdout_seek (type 5) (param i32 i64 i32) (result i64)
    i64.const 0)
  (func $__wasi_syscall_ret (type 1) (param i32) (result i32)
    local.get 0
    i32.eqz
    if  ;; label = @1
      i32.const 0
      return
    end
    call $__errno_location
    local.get 0
    i32.store
    i32.const -1)
  (func $stackSave (type 0) (result i32)
    global.get $__stack_pointer)
  (func $stackRestore (type 3) (param i32)
    local.get 0
    global.set $__stack_pointer)
  (func $stackAlloc (type 1) (param i32) (result i32)
    (local i32)
    global.get $__stack_pointer
    local.get 0
    i32.sub
    i32.const -16
    i32.and
    local.tee 1
    global.set $__stack_pointer
    local.get 1)
  (table (;0;) 8 8 funcref)
  (memory (;0;) 256 256)
  (global $__stack_pointer (mut i32) (i32.const 1069152))
  (export "memory" (memory 0))
  (export "__indirect_function_table" (table 0))
  (export "_start" (func $_start))
  (export "__errno_location" (func $__errno_location))
  (export "stackSave" (func $stackSave))
  (export "stackRestore" (func $stackRestore))
  (export "stackAlloc" (func $stackAlloc))
  (elem (;0;) (i32.const 1) func $__wasm_call_ctors $__stdio_close $__stdio_read $__stdio_seek $__emscripten_stdout_close $__stdio_write $__emscripten_stdout_seek)
  (data $.data (i32.const 1024) "\09")
  (data $.data.1 (i32.const 1036) "\02")
  (data $.data.2 (i32.const 1056) "\03\00\00\00\00\00\00\00\04\00\00\00HH\0f\00\00\04")
  (data $.data.3 (i32.const 1100) "\ff\ff\ff\ff")
  (data $.data.4 (i32.const 1169) "\04\00\00\00\00\00\00\05")
  (data $.data.5 (i32.const 1188) "\05")
  (data $.data.6 (i32.const 1212) "\06\00\00\00\07\00\00\00XL\0f\00\00\04")
  (data $.data.7 (i32.const 1236) "\01")
  (data $.data.8 (i32.const 1252) "\ff\ff\ff\ff\0a")
  (data $.data.9 (i32.const 1320) "\98\04"))
)wasm";
	}
};

int main(int argc, char* argv[]) {
	return bf::transpiler_main<wat_transpile_policy>(argc, argv);
}