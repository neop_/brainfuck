

#include "brainfuck.hpp"
#include <fstream>

int main(int argc, char* argv[]) try {
	std::ifstream code_file{argv[1], std::ios::in};
	if (!code_file) {
		throw std::runtime_error{"could not open file"};
	}

	std::ofstream cpp_file{"/tmp/bf_jitted.cpp"};

	std::ranges::copy(
		std::ranges::istream_view<char>(code_file) |
			std::views::transform(bf::to_token),
		bf::to_ir(bf::to_cpp(cpp_file))
	);

} catch (const std::exception& e) {
	std::puts(e.what());
	return EXIT_FAILURE;
}