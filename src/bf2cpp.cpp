

#include "brainfuck/transpiler_main.hpp"
namespace bf = de07e3fe_e3f8_4858_b44e_aa3f2d4d79cc;

inline constexpr auto cpp_head = R"(#include <cstdio>
int main(){unsigned char mem[1'000'000]={0};auto*p=mem;)";

inline constexpr auto cpp_tail = '}';

template <typename Os>
struct cpp_transpile_policy {
	static auto write_head(Os& os) { os << cpp_head; }
	static auto write_rled_ir(Os& os, bf::ir_point p, int count) {
		using enum bf::ir_point;
		switch (p) {
			case incr: os << "*p+="; break;
			case decr: os << "*p-="; break;
			case right: os << "p+="; break;
			case left: os << "p-="; break;
			default: UNREACHABLE();
		}
		os << count << ';';
	}
	static auto write_read(Os& os) { os << "*p=std::getchar();"; }
	static auto write_write(Os& os) { os << "std::putchar(*p);"; }
	static auto write_zero(Os& os) { os << "*p=0;"; }
	static auto write_whle(Os& os) { os << "while(*p){"; }
	static auto write_wend(Os& os) { os << '}'; }
	static auto write_tail(Os& os) { os << cpp_tail; }
};

template <typename Os>
struct naive_cpp_transpile_policy {
	static auto write_head(Os& os) { os << cpp_head; }
	static auto write_rled_ir(Os& os, bf::ir_point p, int count) {
		using enum bf::ir_point;
		switch (p) {
			case incr:
				for (int i = 0; i < count; ++i) {
					os << "++*p;";
				}
				break;
			case decr:
				for (int i = 0; i < count; ++i) {
					os << "--*p;";
				}
				break;
			case right:
				for (int i = 0; i < count; ++i) {
					os << "++p;";
				}
				break;
			case left:
				for (int i = 0; i < count; ++i) {
					os << "--p;";
				}
				break;
			default: UNREACHABLE();
		}
	}
	static auto write_read(Os& os) { os << "*p=std::getchar();"; }
	static auto write_write(Os& os) { os << "std::putchar(*p);"; }
	static auto write_zero(Os& os) { os << "while(*p)--*p;"; }
	static auto write_whle(Os& os) { os << "while(*p){"; }
	static auto write_wend(Os& os) { os << '}'; }
	static auto write_tail(Os& os) { os << cpp_tail; }
};

int main(int argc, char* argv[]) {
	return bf::transpiler_main<cpp_transpile_policy>(argc, argv);
}